package com.home.pathonmap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    GoogleMap googleMap;
    String url = "https://dl.dropboxusercontent.com/u/5842089/route.txt";
    ArrayList<LatLng> coordList;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                coordList = new ArrayList<>();
                coordList.add(new LatLng(54.79822855179795, 73.09800403602318));
                coordList.add(new LatLng(54.79822855179795, 73.09800403602318));
                coordList.add(new LatLng(54.80243188601439, 73.12412291200368));
                coordList.add(new LatLng(54.8047851105995, 73.13871903205559));
                coordList.add(new LatLng(54.805894803739655, 73.14582222596533));
                coordList.add(new LatLng(54.806705969746346, 73.15072653270813));
                coordList.add(new LatLng(54.809268171637626, 73.16642215642732));
                coordList.add(new LatLng(54.80934901247532, 73.16796579135716));

                PolylineOptions polylineOptions = new PolylineOptions()
                        .addAll(coordList)
                        .color(Color.MAGENTA).width(5);

                googleMap.addPolyline(polylineOptions);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(getCenter(coordList), 12));
            }
        });
        createMapView();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * Initialises the mapview
     */
    private void createMapView() {
        /**
         * Catch the null pointer exception that
         * may be thrown when initialising the map
         */
        try {
            if (null == googleMap) {
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                        R.id.mapView)).getMap();

                /**
                 * If the map is still null after attempted initialisation,
                 * show an error to the user
                 */
                if (null == googleMap) {
                    Toast.makeText(getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException exception) {
            Log.e("PathOnMap" +
                    "", exception.toString());
        }
    }

    /**
     * Adds a marker to the map
     */
    private void addMarker(double x, double y) {

        /** Make sure that the map has been initialised **/
        if (null != googleMap) {
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(x, y))
                    .title("Marker")
                    .draggable(true)
            );
        }
    }

    private LatLng getCenter(List<LatLng> points){
        double la=0;
        double lo=0;
        for (LatLng point:points) {
            la+=point.latitude;
            lo+=point.longitude;
        }
        la/=points.size();
        lo/=points.size();
        return new LatLng(la, lo);
    }
    }
